---
layout: post
title:  "Pivoting a NixOS installation's store to an LVM array"
date:   2018-10-23 10:40:24 -0400
categories: nix
---

# Premise
I have a DigitalOcean server. I'm cheap, so I have the cheapest one. NixOS, while the fact that it never automatically deletes programs is nice, eats lots of disk space. I only have 25GB. So, I go to upgrade the disk space, and... DigitalOcean needs me to migrate my server. I do that. I can't upgrade the root volume. *Yay.* So, I look into converting ext4 to LVM in-place (not exactly possible). Time to pivot it over, I guess.

# Mounting the Volume
I start paying an extra $1/month for a 10GB SSD, and then configure NixOS to mount it. rsync over my store, and... wait. In order to use the store, I have to change the old store. If I change the old store, then I have to copy over the changes, but I can't do that because it'll try to use the broken new store. What do I do?

# systemd Magic
I reboot, and boot into emergency mode with the kernel option `systemd.unit=emergency.target`. From here, whatever keeps me from bind-mounting at runtime (which I tried) isn't set up yet, so I can just bind-mount, and then run `systemctl start multi-user.target` to finish startup. Now, I can configure the bind-mount at startup and...

# Nothing happens
Nothing happens because it still starts booting from the old store. So, I `nix copy` the configuration over, and then switch to it. Reboot again, and...

# Success! (kinda)
Nice! I'm booting from the new store! Use some bind-mount magic to delete the old store, reboot, ***do literally all of this a second time because you forgot to move the kernels to /boot***, and we're good! Now, we need to get the space taken up by the old store into the LVM array.

# Reboot into recovery (no paperclip required)
Poweroff, and then enable the DigitalOcean recovery mode (I'm not crazy enough to try to do this from a ramdisk). Follow the instructions on the [RedHat support website](https://access.redhat.com/articles/1196333) to shrink the partition down 7GB (there was 14G free space), really hope it doesn't corrupt the superblock (partition resizing has bitten me before), and create a new partition with the spare space. Reboot, and we can (hopefully) extend it online.

# Complete! (almost)
Use pvcreate to initialize the new partition, vgextend to add it to the volume group, and `lvextend /dev/mapper/nixstore-nixstorelv +100%FREE` to fill up the volume group. `resize2fs /dev/mapper/nixstore-nixstorelv` to enable the new space while online, and... presto! 17GB for my Nix store!